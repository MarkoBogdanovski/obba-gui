export default function ({ store, redirect }) {
  if (store.state.auth.owner != 1) {
    return redirect('/')
  }
}
