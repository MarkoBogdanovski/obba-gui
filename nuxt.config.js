const webpack = require('webpack')

require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` })
module.exports = {
  head: {
    titleTemplate: '%s - Obba - Obba Shop',
    title: 'Obba Shop - Prodaja Garderobe, Obuće, Aksesoara',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1, shrink-to-fit=no',
      },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
      {
        hid: 'keywords',
        name: 'keywords',
        content:
          'rsd, detalji, onlajn prodaja garderobe, kupi, prodaj, ženska, muška, aksesoari, Odeća, Obuća, Oprema, Garderoba, Majice, Patike, Papuče, Trenerke, Duksevi, Kompleti, Dodaci, Aksesoari, Naocare, Naočare, Nakit, Narukvice, Rukavice, Ogrlice, Maske za telefon, Telefon, Maske',
      },
      {
        hid: 'yandex-verification',
        name: 'yandex-verification',
        content: 'e52f68bb788136f0',
      },
      {
        rel: 'preconnect',
        href: 'https://fonts.gstatic.com',
      },
    ],
  },

  loading: { color: '#0055ff' },

  pwa: {
    manifest: {
      name: 'Obba Shop',
      short_name: 'Obba',
      start_url: '/',
      theme_color: '#3c4d69',
      background_color: '#FFF',
      lang: 'sr',
      display: 'standalone',
      crossorigin: 'use-credentials',
    },
    workbox: {
      importScripts: ['workbox-sw.js'],
      //dev: true,
    },
  },

  sitemap: {
    hostname: 'https://obba.online/',
  },

  auth: {
    strategies: {
      local: false,
      password_grant: {
        _scheme: 'local',
        endpoints: {
          login: {
            url: '/auth/login',
            method: 'post',
            propertyName: 'data.token',
          },
          logout: false,
        },

        tokenType: 'Bearer',
      },
    },
  },

  toast: {
    position: 'bottom-left',
  },

  components: [
    {
      path: '~/components/',
      extensions: ['vue'],
    },
    {
      path: '~/layouts/',
      extensions: ['vue'],
    },
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/auth',
    'nuxt-feature-toggle',
    'vue-social-sharing/nuxt',
    'portal-vue/nuxt',
    'nuxt-svg-loader',
    // "@nuxtjs/google-adsense",
    '@nuxtjs/gtm',
  ],

  /*
   ** Feature toggler configuration
   */
  featureToggle: {
    toggles: {
      analyitics: false,
    },
  },

  buildModules: [
    ['@nuxtjs/dotenv', { filename: `.env.${process.env.NODE_ENV}` }],
    '@nuxtjs/google-fonts',
    '@nuxtjs/fontawesome',
    '@nuxtjs/toast',
    '@nuxtjs/google-analytics',
    // "@nuxtjs/laravel-echo",
  ],

  googleFonts: {
    families: {
      Inter: [300, 400, 500, 600, 700],
    },
    display: 'swap',
  },

  echo: {
    plugins: ['~/plugins/echo.js'],
    broadcaster: 'pusher',
    //authEndpoint: process.env.AXIOS_API_URL + "broadcasting/auth",
    cluster: process.env.PUSHER_APP_CLUSTER,
    secret: process.env.PUSHER_APP_SECRET,
    key: process.env.PUSHER_APP_KEY,
    encrypted: true,
    authModule: true,
  },

  fontawesome: {
    component: 'fa',
    suffix: true,
    icons: {
      brands: [
        'faFacebookMessenger',
        'faPinterest',
        'faViber',
        'faWhatsapp',
        'faAlgolia',
      ],
    },
  },

  publicRuntimeConfig: {
    /*
     ** Axios and Auth module configuration
     */
    axios: {
      baseURL: process.env.AXIOS_API_URL,
      crossDomain: true,
    },
    gtm: {
      id: process.env.GOOGLE_TAG_MANAGER_ID,
    },
    googleAnalytics: {
      id: process.env.GOOGLE_ANALYTICS_ID,
      debug: {
        enabled: false,
        sendHitTask: false,
      },
    },
    description: process.env.npm_package_description,
    apiUrl: process.env.AXIOS_API_URL,
    baseUrl: process.env.BASE_URL,
    storageUrl: process.env.STORAGE_URL,
  },

  env: {
    description: process.env.npm_package_description,
    appUrl: process.env.APP_URL,
    apiUrl: process.env.AXIOS_API_URL,
    baseUrl: process.env.BASE_URL,
    storageUrl: process.env.STORAGE_URL,
  },

  css: ['~/assets/scss/bootstrap.scss', '~/assets/scss/ui.scss'],

  plugins: [
    { src: '~/plugins/localStorage.js', mode: 'client' },
    { src: '~/plugins/vuex-shared-mutations.js', mode: 'client' },
    { src: '~/plugins/directives.js', mode: 'client' },
    { src: '~/plugins/bootstrap-icons.js', mode: 'client' },
    { src: '~/plugins/bootstrap.js', mode: 'client' },
    { src: '~/plugins/v-grid.js', mode: 'client' },
    { src: '~/plugins/v-input.js', mode: 'client' },
    { src: '~/plugins/tinySlider.js', mode: 'client' },
    { src: '~/plugins/lazysizes.js', mode: 'client' },
  ],

  /*
   ** Build configuration
   */
  build: {
    analyze: false,
    vue: {
      config: {
        silent: true,
        performance: true,
      },
    },
    html: {
      minify: {
        collapseBooleanAttributes: true,
        decodeEntities: true,
        minifyCSS: true,
        minifyJS: true,
      },
    },
    minimize: true,
    splitChunks: {
      chunks: 'all',
      automaticNameDelimiter: '.',
      name: undefined,
      cacheGroups: {},
    },
    transpile: ['eslint-loader', 'vue'],
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        _: 'lodash',
        Popper: 'popper.js',
      }),
    ],
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.optimization.splitChunks.maxSize = 200000
        config.devtool = isClient ? 'source-map' : 'inline-source-map'
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        })
      }
    },
  },
}
