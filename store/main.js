export const state = () => ({
  categories: [],
  nav: [],
  search: [],
  cart: [],
  shopPopover: true,
})

export const mutations = {
  setCategories(state, payload) {
    state.categories = payload
    return state
  },
  addCategory(state, payload) {
    state.categories.push(payload)
    return state
  },
  parseNav(state, payload) {
    let categories = payload
    let parent = payload.filter((category) => category.parent === null)

    _.each(categories, function (category) {
      if (!_.isNull(category.parent)) {
        let indexOf = parent.findIndex((x) => x.slug === category.parent)
        if (_.isUndefined(parent[indexOf]['childs'])) {
          parent[indexOf]['childs'] = []
        }
        parent[indexOf]['childs'].push(category)
      }
    })

    state.nav = parent
    return state
  },
  saveSearch(state, payload) {
    let duplicates = state.search.filter(
      (item) =>
        item.search === payload.search && item.category === payload.category
    )
    if (duplicates.length < 1) {
      state.search.unshift(payload)
    }
    return state
  },
  clearSearchHistory(state) {
    state.search = []
    return state
  },

  setCart(state, payload) {
    state.cart = payload
    return state
  },
  addToCart(state, payload) {
    let items = [...state.cart]

    items.push(payload)

    state.cart = items
    return state
  },
  removeFromCart(state, payload) {
    state.cart.splice(payload, 1)
    return state
  },
  emptyCart(state) {
    state.cart = []
    return state
  },
  toggleShopPopover(state) {
    let show = state.shopPopover
    state.shopPopover = !show
    return state
  },
}

export const getters = {
  getCategories(state) {
    return state.categories
  },
  getById: (state) => (id) => {
    return state.categories.filter((item) => item._id === id)
  },
  getBySlug: (state) => (slug) => {
    return state.categories.filter((item) => item.slug === slug)
  },
  getNav: (state) => {
    return state.nav
  },
  getSearch(state) {
    return state.search
  },
  getLastSearch(state) {
    if (state.search.length > 0) {
      return state.search[0]
    }
    return false
  },
  getCart(state) {
    return state.cart
  },
}

export const actions = {
  async loadCategories(context) {
    try {
      const res = await this.$axios.$get('categories')
      context.commit('setCategories', res)
      context.commit('parseNav', res)
      return res
    } catch (e) {
      this.error = e.response.data.message
    }
  },
  async finishOrder(context, items) {
    try {
      let data = new Object()
      data.order = items

      const res = await this.$axios.$post('/order', data)

      if (res.statusCode == 200) {
        context.commit('emptyCart')
      }
    } catch (e) {
      this.error = e.response.data.message
    }
  },
  async addToCart(context, item) {
    try {
      context.commit('addToCart', item)
    } catch (e) {
      this.error = e
    }
  },
  async toggleShopPopover(context) {
    context.commit('toggleShopPopover')
  },
}
