export const state = () => ({
  shops: [],
  myShop: [],
  id: [],
})

export const mutations = {
  set(state, payload) {
    state.shops = payload
  },
  add(state, payload) {
    if (!state.id.includes(payload._id)) {
      state.shops.push(payload)
      state.id.push(payload._id)
    }
  },
  setMyShop(state, payload) {
    state.myShop = payload
  },
  removeProduct(state, id) {
    let index = state.myShop.products.findIndex((item) => item._id == id)
    state.myShop.products.splice(index, 1)
  },
}

export const getters = {
  get(state) {
    return state.shops
  },
  getByName: (state) => (name) => {
    return state.shops.filter((item) => item.name === name)
  },
  getById: (state) => (id) => {
    return state.shops.filter((item) => item._id === id)
  },
  getMyShop: (state) => {
    return state.myShop
  },
  getOrders: (state) => {
    if (!_.isEmpty(state.myShop.orders)) {
      return state.myShop.orders.orders[0]
    }
    return false
  },
  getNewOrders: (state) => {
    if (!_.isEmpty(state.myShop.orders)) {
      return state.myShop.orders.filter((order) => order.status == 1)
    }
    return false
  },
}

export const actions = {
  async load(context) {
    try {
      const res = await this.$axios.$get('shops')
      Object.keys(res.data).forEach((i) => {
        context.commit('add', res.data[i])
      })
    } catch (e) {
      this.error = e
    }
  },
  async fetchShop(context) {
    try {
      const res = await this.$axios.$get('shop')
      context.commit('setMyShop', res.data.shop)
      return res
    } catch (e) {
      this.error = e
    }
  },
  async fetchByUser(context) {
    try {
      const res = await this.$axios.$get('shop')
      context.commit('add', res.data)
    } catch (e) {
      this.error = e
    }
  },
  async removeProduct(context, id) {
    try {
      const res = await this.$axios.$delete('product/' + id)
      if (res.statusCode == 200) {
        context.commit('removeProduct', id)
      }
    } catch (e) {
      this.error = e
    }
  },
}
