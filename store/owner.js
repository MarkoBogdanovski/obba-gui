export const state = () => ({
  changes: [],
})

export const mutations = {
  updateNotifications(state, payload) {
    state.auth.user.notifications = payload.notifications
    state.auth.user.unread_notifications = payload.unread_notifications
  },
}

export const getters = {
  getUser(state) {
    return state.auth.user
  },
  getAddress(state) {
    return state.auth.user.address
  },
  getAddressById: (state) => (id) => {
    return state.auth.user.address.filter((item) => item._id === id)
  },
  getDefaultAddress(state) {
    let adr = state.auth.user.address.filter((item) => item.default === true)

    if (adr.length === 0) {
      let adrCount = state.auth.user.address.length - 1
      return state.auth.user.address[adrCount]
    }

    return adr[0]
  },
}

export const actions = {
  async updateOrderStatus(context, data) {
    try {
      const res = await this.$axios.$patch('order/' + data._id, {
        status: data.status,
      })

      return res.data.order
    } catch (e) {
      this.error = e.response.data.message
    }
  },
  /*async fetchNotifications(context) {
    try {
      const res = await this.$axios.$get("notifications")
      context.commit("updateNotifications", res.data)
    } catch (e) {
      this.error = e.response.data.message
    }
  },*/
  async convertToOwner(context) {
    context.commit('owner', true)
  },
  async fetchAddress(context) {
    try {
      const res = await this.$axios.$get('address')
      context.commit('addAddress', res.data)

      return res.data
    } catch (e) {
      this.error = e.response.data.message
    }
  },
  async removeAddress(context, id) {
    try {
      await this.$axios.$delete('address/' + id)
      context.commit('removeAddress', id)
    } catch (e) {
      this.error = e
    }
  },
}
