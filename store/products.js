export const state = () => ({
  products: [],
  pids: [],
  loading: false,
})

export const mutations = {
  set(state, payload) {
    state.products = payload
  },
  add(state, payload) {
    if (!state.pids.includes(payload._id)) {
      state.products.push(payload)
      state.pids.push(payload._id)
    }
  },
  setLoading(state, payload) {
    state.loading = payload
  },
}

export const getters = {
  get(state) {
    return state.products
  },
  getById: (state) => (id) => {
    return state.products.filter((item) => item._id === id)
  },
  getByShopId: (state) => (id) => {
    return state.products.filter((item) => item.shop_id === id)
  },
  isLoading(state) {
    return state.loading
  },
}

export const actions = {
  async load(context) {
    context.commit('setLoading', true)
    try {
      const res = await this.$axios.$get('products?limit=48')
      Object.keys(res.data).forEach((i) => {
        context.commit('add', res.data[i])
      })
      context.commit('setLoading', false)
    } catch (e) {
      this.error = e.response.data.message
    }
  },
  async loadMore(context, page) {
    context.commit('setLoading', true)
    try {
      const res = await this.$axios.$get('products?page=' + page)
      Object.keys(res.data).forEach((i) => {
        context.commit('add', res.data[i])
      })
      context.commit('setLoading', false)
    } catch (e) {
      this.error = e
    }
  },
  async fetchProduct(context, id) {
    context.commit('setLoading', true)
    try {
      const res = await this.$axios.$get('products/' + id) // @TODO Remove static url
      context.commit('add', res.data.product)
      context.commit('setLoading', false)

      return res.data
    } catch (e) {
      this.error = e
    }
  },
  async fetchByCategory(context, slug) {
    context.commit('setLoading', true)
    try {
      const res = await this.$axios.$get('category/' + slug + '/products')
      context.commit('setLoading', false)

      return res.data
    } catch (e) {
      this.error = e
    }
  },
}
