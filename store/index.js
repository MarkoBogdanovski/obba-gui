export const mutations = {
  set(state, payload) {
    state.auth.user = payload
  },
  owner(state, payload) {
    state.auth.user.owner = payload
  },
  addAddress(state, payload) {
    state.auth.user.address = payload
  },
  removeAddress(state, id) {
    let index = state.auth.user.address.findIndex((item) => item._id == id)
    state.auth.user.address.splice(index, 1)
  },
  updateNotifications(state, payload) {
    state.auth.user.notifications = payload.notifications
    state.auth.user.unread_notifications = payload.unread_notifications
  },
  setError(state, payload) {
    this.$toast.error(payload)
  },
}

export const getters = {
  getUser(state) {
    return state.auth.user
  },
  getAddress(state) {
    return state.auth.user.address
  },
  getAddressById: (state) => (id) => {
    return state.auth.user.address.filter((item) => item._id === id)
  },
  getDefaultAddress(state) {
    let adr = state.auth.user.address.filter((item) => item.default === true)
    let adrCount = state.auth.user.address.length - 1

    if (adrCount >= 0) {
      return state.auth.user.address[adrCount]
    }

    return adr[0]
  },
}

export const actions = {
  async fetchUser(context) {
    try {
      const res = await this.$axios.$get('auth/user')

      if (res.statusCode === 200) {
        context.commit('set', res.data)
      }
    } catch (e) {
      context.commit('setError', e)
    }
  },
  async fetchNotifications(context) {
    try {
      const res = await this.$axios.$get('notifications')
      if (res.statusCode === 200) {
        return context.commit('updateNotifications', res.data)
      }
    } catch (e) {
      //
    }
  },
  async readNotification(context, data) {
    try {
      const res = await this.$axios.$post('notification/' + data)
      if (res.statusCode === 200) {
        context.commit('updateNotifications', res.data)
      }
    } catch (e) {
      context.commit('setError', e)
    }
  },
  async convertToOwner(context) {
    context.commit('owner', true)
  },
  async fetchAddress(context) {
    try {
      const res = await this.$axios.$get('address')

      if (res.statusCode === 200) {
        context.commit('addAddress', res.data)
        return res.data
      }
    } catch (e) {
      context.commit('setError', e)
    }
  },
  async removeAddress(context, id) {
    try {
      const res = await this.$axios.$delete('address/' + id)
      if (res.statusCode == 200) {
        context.commit('removeAddress', id)
      }
    } catch (e) {
      context.commit('setError', e)
    }
  },
}
