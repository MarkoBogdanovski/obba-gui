import Vue from 'vue'
import vTinySlider from 'vue-tiny-slider'

const VueTinySlider = {
  install(Vue) {
    Vue.component('TinySlider', vTinySlider)
  },
}

Vue.use(VueTinySlider)
