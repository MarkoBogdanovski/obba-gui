import Vue from 'vue'
import { VuejsDatatableFactory } from 'vuejs-datatable'

VuejsDatatableFactory.useDefaultType(false).registerTableType(
  'datatable',
  (tableType) =>
    tableType.mergeSettings({
      table: {
        class: 'table table-hover table-striped',
        sorting: {
          sortAsc:
            '<i class="fas fa-sort-amount-up" title="Sort ascending"></i>',
          sortDesc:
            '<i class="fas fa-sort-amount-down" title="Sort descending"></i>',
          sortNone: '<i class="fas fa-sort" title="Sort"></i>',
        },
      },
      pager: {
        classes: {
          pager: 'pagination text-center',
          selected: 'active',
        },
        icons: {
          next: '<i class="fas fa-chevron-right" title="Next page"></i>',
          previous: '<i class="fas fa-chevron-left" title="Previous page"></i>',
        },
      },
    })
)

Vue.use(VuejsDatatableFactory)
