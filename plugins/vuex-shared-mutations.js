import shareMutations from 'vuex-shared-mutations'

export default ({ store }) => {
  // eslint-disable-next-line no-unused-vars
  window.onNuxtReady((nuxt) => {
    shareMutations({
      predicate: ['main/addToCart', 'main/removeFromCart', 'main/emptyCart'],
    })(store)
  })
}
