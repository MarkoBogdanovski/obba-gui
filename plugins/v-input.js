import Vue from 'vue'
import InputTag from 'vue-input-tag'

const InputTags = {
  install(Vue) {
    Vue.component('InputTag', InputTag)
  },
}

Vue.use(InputTags)
